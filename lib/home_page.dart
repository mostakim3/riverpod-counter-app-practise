import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod/riverpod.dart';

final counterStateProvider = StateProvider<int>((ref) {
  return 0;
});
class MyHomePage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final counter = ref.watch(counterStateProvider);
    return Scaffold(
      body: Center(
        child:  Text(
              'Value: $counter',
              style: Theme.of(context).textTheme.headline4,
            )
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FloatingActionButton(
              onPressed: () => ref.read(counterStateProvider.state).state++,
              child: Icon(Icons.add),
            ),
            Expanded(child: Container()),
            FloatingActionButton(
              onPressed: () => ref.read(counterStateProvider.state).state--,
              child: Icon(Icons.remove),
            ),
          ],
        ),
      )
    );
  }
}